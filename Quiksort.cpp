/*
 * Escuela:
 * Curso:
 * Profesor:
 * alumno:
 * Fecha:
 * Descripcion:
 *
 *
 */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int cont=0;
int identificador = 1;

struct Nodo{
	int dato;
	int id;
	struct Nodo* siguiente;
	struct Nodo* anterior;
};

typedef struct Nodo* PLista;


PLista CrearNodo(int dato,int id){
	PLista p = new (struct Nodo);
	p->dato=dato;
	p->id=id;
	p->siguiente=NULL;
	p->anterior=NULL;
	return p;
}

void RecorrerLista(PLista &lista){

	PLista p = lista;
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;

	}
	cout<<"null"<<endl;

}

void InsertarPrincipio(PLista &lista,int dato, int id){
	PLista nuevo =CrearNodo(dato, id);

	if(lista == NULL){

		lista=nuevo;

	}else{
		nuevo->siguiente = lista;
      nuevo->anterior = NULL;
		lista->anterior=nuevo;
		lista=nuevo;
	}
	cont=cont+1;
}

void InsertarFinal(PLista &lista,int dato, int id){
	PLista nuevo=CrearNodo(dato, id);
	if(lista == NULL){
		lista = nuevo;
	}else{
		PLista aux=lista;

		while(aux->siguiente!=NULL){
			aux=aux->siguiente;
		}
		aux->siguiente=nuevo;
		nuevo->anterior=aux;
		nuevo->siguiente=NULL;

	}
	cont = cont+1;
}

void InsertarPorPosicion(PLista &lista,int dato,int id, int pos){
	PLista nuevo=CrearNodo(dato, id);
	if(lista == NULL){
		cout<<"La lista esta vacio no se puede agregar"<<endl;
	}else{
		if(pos <= cont && pos>0){
			if(pos==1){
				InsertarPrincipio(lista,dato,id);
			}else{
				int n=1;
				PLista aux=lista;
				while(n!= pos){
					aux=aux->siguiente;
					n++;
				}
				nuevo->siguiente=aux;
				nuevo->anterior=aux->anterior;
				aux->anterior=nuevo;
				nuevo->anterior->siguiente=nuevo;
				cont = cont+1;
			}
		}else{
			cout<<"posicion invalida"<<endl;
		}
	}
}


void QuickSort(PLista &lista, PLista &ultimo){
    PLista p = lista;
    PLista pivote;
    int i = 1;
    int longitud=1;
    while(p != ultimo){
        p = p->siguiente;
        longitud++;
    }
   
    if(longitud != 1){
	
    p = lista;
    while(i <= (longitud/2)){
        p = p->siguiente;
        i++;
    }
    pivote = p;
   
    PLista inicio = lista;
   
    PLista fin;
    i=1;
    p = lista;
    while(i < longitud){
        p = p->siguiente;
        i++;
    }
    fin = p;
   
    int aux;
    while(fin != pivote || inicio != pivote){
	
        while(fin != pivote && inicio != pivote){
            if(inicio->dato > pivote->dato && fin->dato < pivote->dato){
                aux = inicio->dato;
                inicio->dato = fin->dato;
                fin->dato = aux;
                inicio = inicio->siguiente;
                fin = fin->anterior;
                
            }
            else{
                if(inicio->dato <= pivote->dato){
                    inicio = inicio->siguiente;
                }
                if(fin->dato > pivote->dato){
                    fin = fin->anterior;
                }
            }
        }

        if(fin == pivote){
            if(inicio->dato > pivote->dato){
                aux = pivote->dato;
                pivote->dato = inicio->dato;
                inicio->dato = aux;
                pivote = inicio;
                fin = fin->anterior;
            }
            else{
            	if(inicio != pivote){
            		inicio = inicio->siguiente;
				}
                	
            }
        }

        if(inicio == pivote){
            if(fin->dato < pivote->dato){
                aux = pivote->dato;
                pivote->dato = fin->dato;
                fin->dato = aux;
                pivote = fin;
                inicio = inicio->siguiente;
            }
            else{
            	if(fin != pivote){
            		fin = fin->anterior;
				}
                
            }
        }

    }
	
    p =lista;
    if(pivote->siguiente != NULL && pivote != ultimo){
    	QuickSort(pivote->siguiente, ultimo);
	}
    if(pivote->anterior != NULL && pivote != ultimo){
    	QuickSort(p,pivote->anterior);
	}
    }
}

int menu(){
    int opc;
       cout<<"Menu de Busquedas:"<<"--------------"<<endl;
       cout<<"1. Llenado de lista : "<<endl;
        cout<<"2. Qick Sort"<<endl;
       cout<<"3. Mostrar lista"<<endl;
       cout<<"0. Salir"<<endl;
       cout<< "Ingrese su opcion: ";
       cin >>opc;

    return  opc;

    }


int main()
{
   PLista lista = NULL;
	PLista pos =NULL;
   int dato;
   char rpt;
   int opc;
   do {
         opc = menu();
         switch(opc){
               case 1:
                        int dato;
                        do{
                        cout<<"ingrese el dato:";
                        cin>>dato;
                        InsertarFinal(lista,dato,identificador);
                        RecorrerLista(lista);
                        cout<<"Desea continuar?S/N:";
                        cin>>rpt;
                        identificador++;
                        }while(rpt=='s' || rpt=='S');

                        break;
                case 2:
                		pos = lista;
                		while(pos->siguiente != NULL){
                			pos = pos->siguiente;
						}
                        QuickSort(lista,pos);
                        RecorrerLista(lista);
						break;
               case 3:

                        RecorrerLista(lista);

                        break;


            }
            system("pause");  system("cls");
        }while(opc!=0);




   system ("pause");

   return(0);

}

